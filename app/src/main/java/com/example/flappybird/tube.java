package com.example.flappybird;

import java.util.Random;

public class tube {
    //tube x = Tube X coordinate,topTubeOffsetY =top tube bottom edge coordinate
    private int tubex,topTubeOffsetY;
    private Random random;
    private int tubecolor;
    public  tube(int tubex,int topTubeOffsetY){
        this.tubex = tubex;
        this.topTubeOffsetY = topTubeOffsetY;
        random = new Random();

    }
    public void setTubeColor(){
        tubecolor = random.nextInt(2);
    }
    public int getTubeColor(){
        return tubecolor;
    }
    public int getTopTubeOffsetY(){
        return topTubeOffsetY;
    }
    public int getTubex(){
        return tubex;
    }
    public int getTopTubeY(){
        return topTubeOffsetY - AppConstants.getBitmapBank().getTubeHeight();
    }
    public int getBottomTubeY(){
        return topTubeOffsetY + AppConstants.gapBetweenTopAndBottomTubes;
    }

    public void setTubex(int tubex) {
        this.tubex = tubex;
    }

    public void setTopTubeOffsetY(int topTubeOffsetY) {
        this.topTubeOffsetY = topTubeOffsetY;
    }

}
