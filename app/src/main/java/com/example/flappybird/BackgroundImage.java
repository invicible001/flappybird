package com.example.flappybird;

public class BackgroundImage {
    private int backgroundImageX,backgroundImageY,backgroundImageVelocity;



    public BackgroundImage(){
        backgroundImageVelocity = 3;
        backgroundImageX = 0;
        backgroundImageY = 0;

    }
    //Getter method for X cooordinates
    public int getX(){
        return backgroundImageX;

    }
    //Getter method for getting Y cooridante
    public int getY(){

        return backgroundImageY;
    }

    //Setter method for setting x coordinate
    public void setX(int backgroundImageX){
        this.backgroundImageX = backgroundImageX;

    }
    //setter method for coordinate of Y
    public void setY(int backgroundImageY){
        this.backgroundImageY = backgroundImageY;
    }
    //Getter method for image velocity
    public int getVelocity(){
        return backgroundImageVelocity;
    }
}
