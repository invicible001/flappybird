package com.example.flappybird;

public class Bird {
    private int birdX,birdY,currentFrame,velocity;
    public static int maxFrame;
    public Bird() {
        birdX = AppConstants.SCREEN_WIDTH / 2 - AppConstants.getBitmapBank().getBirdWidth() / 2;
        birdY = AppConstants.SCREEN_HEIGHT / 2 - AppConstants.getBitmapBank().getBirdHeight() / 2;
        currentFrame = 0;
        maxFrame = 3;
        velocity = 0;
    }

        //Getter method for velocity

        public int getVelocity(){
            return velocity;
        }
        //setter method for velocity


    public void setVelocity(int velocity) {
        this.velocity = velocity;
    }

    //Getter method for creating frame
public int getCurrentFrame(){
        return currentFrame;
}
//Setter method for current frame


    public void setCurrentFrame(int currentFrame) {
        this.currentFrame = currentFrame;
    }

    //Getter method for getting x-coordinate
    public int getX(){
        return birdX;
    }
    //Getter method for y coordinate

    public int getY(){
        return birdY;
    }

    //Setter methods for the coordinate


    public void setX(int birdX) {
        this.birdX = birdX;
    }
    public void setY(int birdY){
        this.birdY = birdY;
    }
}
