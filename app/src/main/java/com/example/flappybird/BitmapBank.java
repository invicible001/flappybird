package com.example.flappybird;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class BitmapBank {
    Bitmap redTubeBottom,redTubeTop;
    Bitmap background;
    Bitmap[] bird;
    Bitmap tubetop,tubebottom;

    public BitmapBank(Resources res) {
        redTubeTop = BitmapFactory.decodeResource(res , R.drawable.redtubetop);
        redTubeBottom = BitmapFactory.decodeResource(res , R.drawable.redtubebottom);
        tubetop = BitmapFactory.decodeResource(res,R.drawable.tubetop);
        tubebottom = BitmapFactory.decodeResource(res,R.drawable.tubebottom);
        background = BitmapFactory.decodeResource(res, R.drawable.background);
        background = ScaleImage(background);
        //background = scaleImage(background);
        bird = new Bitmap[4];
        bird[0] = BitmapFactory.decodeResource(res,R.drawable.bird1);
        bird[1] = BitmapFactory.decodeResource(res,R.drawable.bird2);
        bird[2] = BitmapFactory.decodeResource(res,R.drawable.bird3);
        bird[3] = BitmapFactory.decodeResource(res,R.drawable.bird4);
    }
    public Bitmap getBird(int frame){
        return bird[frame];

    }
    public int getBirdWidth(){
        return bird[0].getWidth();
    }
    public  int getBirdHeight(){
        return  bird[0].getHeight();
    }
    //Return Tube top

    public Bitmap getTubetop() {
        return tubetop;
    }
    //Return tube bottom

    public Bitmap getTubebottom() {
        return tubebottom;
    }
    //Returntubetopred
    public Bitmap getRedTubeTop(){
        return redTubeTop;
    }

    //Return red tube bottom
    public Bitmap getRedTubeBottom(){
        return redTubeBottom;
    }
//Return tube width
    public int getTubeWidth(){
        return tubetop.getWidth();
    }
    //Return tubeHeight
    public int getTubeHeight(){
        return tubetop.getHeight();
    }

    //Return Bitmapbackground

    public Bitmap getBackground() {
        return background;
    }
    //Return background width

    public int getbackgroundwidth() {
        return background.getWidth();
    }

    //Return backgroundheight
    public int getbackgroundheight(){
        return background.getHeight();
    }

    public Bitmap ScaleImage(Bitmap bitmap){
        float widthHeightRatio = getbackgroundwidth() / getbackgroundheight();
        int backgroundscaledwidth = (int) widthHeightRatio *AppConstants.SCREEN_HEIGHT;
        return Bitmap.createScaledBitmap(bitmap,backgroundscaledwidth,AppConstants.SCREEN_HEIGHT,false);
    }
}
