package com.example.flappybird;

import android.graphics.Canvas;
import android.os.SystemClock;
import android.util.Log;
import android.view.SurfaceHolder;

public class GameThread extends Thread {
    SurfaceHolder surfaceHolder;//SurfaceHolder object
    boolean isRunning;//Flag to detect whether thread is running
    long startTime,loopTime;//loop start and loop duration
    long Delay = 33;//Delay in ms

    public GameThread(SurfaceHolder surfaceHolder){
        this.surfaceHolder = surfaceHolder;
        isRunning = true;

    }

    @Override
    public void run() {
        //looping until the boolean is false
        while (isRunning) {
            startTime = SystemClock.uptimeMillis();

            //locking the canvas
            Canvas canvas = surfaceHolder.lockCanvas(null);
            if (canvas !=null){
                synchronized (surfaceHolder){
                    AppConstants.getGameEngine().updateAndDrawBackgroundImage(canvas);
                    AppConstants.getGameEngine().updateAndDrawBird(canvas);
                    AppConstants.getGameEngine().updateanddrawtubes(canvas);

                    //unlocking the canvas
                    surfaceHolder.unlockCanvasAndPost(canvas);
                }
            }
        }
        //looptime
        loopTime = SystemClock.uptimeMillis() - startTime;
        if (loopTime<Delay){
            try {
                Thread.sleep(Delay - loopTime);
            }catch (InterruptedException e){
                Log.e("Interrupted", "Interrupted while sleeping");
            }
        }
    }
    //Return whether the thread is running
    public boolean isRunning(){

        return isRunning;
    }
    //sets the thread state,false = stopped ,true =running
    public void setIsRunning(boolean state){
        isRunning = state;
    }
}
