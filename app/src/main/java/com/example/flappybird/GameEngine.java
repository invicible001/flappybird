package com.example.flappybird;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;

import java.util.ArrayList;
import java.util.Random;

public class GameEngine {
    static int gamestate;
    Bird bird;
    int currentFrame;
    BackgroundImage backgroundImage;
    ArrayList<tube> tubes;
    Random random;
    int score;//Keeptrack of the score
    int scoringTube;//keeps track of the scoring tube
    Paint scorepaint;

    public GameEngine() {
        backgroundImage = new BackgroundImage();
        bird = new Bird();
        //0 = notstarted
        //1 = playing
        //2 = game over
        gamestate = 0;
        tubes = new ArrayList<>();
        random = new Random();
        for (int i = 0; i < AppConstants.numberOfTubes; i++) {
            int tubex = AppConstants.SCREEN_WIDTH + i * AppConstants.distanceBetweenTubes;
            //GettopTubeOffsetY
            int topTubeOffsetY = AppConstants.mayTubeOffsetY + random.nextInt(AppConstants.mayTubeOffsetY - AppConstants.minTubeOffsetY + 1);
            //create tube objects
            tube Tube = new tube(tubex, topTubeOffsetY);
            tubes.add(Tube);
        }
        score = 0;
        scoringTube = 0;
        scorepaint = new Paint();
        scorepaint.setColor(Color.RED);
        scorepaint.setTextSize(100);
        scorepaint.setTextAlign(Paint.Align.LEFT);


    }

    public void updateanddrawtubes(Canvas canvas) {
        if (gamestate == 1) {
            if ((tubes.get(scoringTube).getTubex() < bird.getX() + AppConstants.getBitmapBank().getBirdWidth())
                    && (tubes.get(scoringTube).getTopTubeOffsetY() > bird.getY()
                    || tubes.get(scoringTube).getBottomTubeY() < (bird.getY() + AppConstants.getBitmapBank().getBirdHeight()))){
                //Go to gameoverscreen
                gamestate = 2;
                Log.d("game", "Over");
            }
            else if (tubes.get(scoringTube).getTubex() < bird.getX() - AppConstants.getBitmapBank().getTubeWidth()) {
                score++;
                scoringTube++;
                if (scoringTube > AppConstants.numberOfTubes - 1) {
                    scoringTube = 0;
                }
            }
            for (int i = 0; i < AppConstants.numberOfTubes; i++) {
                if (tubes.get(i).getTubex() < -AppConstants.getBitmapBank().getTubeWidth()) {
                    tubes.get(i).setTubex(tubes.get(i).getTubex() + AppConstants.numberOfTubes * AppConstants.distanceBetweenTubes);
                    int topTubeOffsetY = AppConstants.minTubeOffsetY + random.nextInt(AppConstants.mayTubeOffsetY - AppConstants.minTubeOffsetY + 1);
                    tubes.get(i).setTopTubeOffsetY(topTubeOffsetY);
                    tubes.get(i).setTubeColor();
                }
                tubes.get(i).setTubex(tubes.get(i).getTubex() - AppConstants.tubeVelocity);
                if (tubes.get(i).getTubeColor() == 0) {
                    canvas.drawBitmap(AppConstants.getBitmapBank().getTubetop(), tubes.get(i).getTubex(), tubes.get(i).getTopTubeY(), null);
                    canvas.drawBitmap(AppConstants.getBitmapBank().getTubebottom(), tubes.get(i).getTubex(), tubes.get(i).getBottomTubeY(), null);

                } else {
                    canvas.drawBitmap(AppConstants.getBitmapBank().getRedTubeTop(), tubes.get(i).getTubex(), tubes.get(i).getTopTubeY(), null);
                    canvas.drawBitmap(AppConstants.getBitmapBank().getRedTubeBottom(), tubes.get(i).getTubex(), tubes.get(i).getBottomTubeY(), null);
                }
            }
            canvas.drawText("Score" + score, 0, 110, scorepaint);

        }
    }

    public void updateAndDrawBackgroundImage(Canvas canvas) {

        backgroundImage.setX(backgroundImage.getX() - backgroundImage.getVelocity());
        if (backgroundImage.getX() < -AppConstants.getBitmapBank().getbackgroundwidth()) {
            backgroundImage.setX(0);
            if (backgroundImage.getY() < -AppConstants.getBitmapBank().getbackgroundheight()) {
                backgroundImage.setY(0);
            }

        }
        canvas.drawBitmap(AppConstants.getBitmapBank().getBackground(), backgroundImage.getX(), backgroundImage.getY(), null);
        if (backgroundImage.getX() < -(AppConstants.getBitmapBank().getbackgroundwidth() - AppConstants.SCREEN_WIDTH)) {
            canvas.drawBitmap(AppConstants.getBitmapBank().getBackground(), backgroundImage.getX() + AppConstants.getBitmapBank().getbackgroundwidth(), backgroundImage.getY(), null);
        }
    }

    public void updateAndDrawBird(Canvas canvas) {
        if (gamestate == 1) {

            if (bird.getY() < (AppConstants.SCREEN_HEIGHT - AppConstants.getBitmapBank().getBirdHeight()) | bird.getVelocity() < 0) {
                bird.setVelocity(bird.getVelocity() + AppConstants.gravity);
                bird.setY(bird.getY() + bird.getVelocity());

            }
        }
        int currentFrame = bird.getCurrentFrame();
        canvas.drawBitmap(AppConstants.getBitmapBank().getBird(currentFrame), bird.getX(), bird.getY(), null);
        currentFrame++;
        //if it exceeds max frame it re initializes to 0
        if (currentFrame > bird.maxFrame) {
            currentFrame = 0;
            bird.setCurrentFrame(currentFrame);
        }

    }
}
