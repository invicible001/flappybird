package com.example.flappybird;

import android.content.Context;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.WindowManager;

public class AppConstants {

    static int SCREEN_WIDTH,SCREEN_HEIGHT;
    static BitmapBank bitmapBank;//bitmapbank object reference
    static GameEngine gameEngine;//Game engine object reference
    static int gravity;
    static  int velocity_Jumped;
    static int gapBetweenTopAndBottomTubes;
    static int numberOfTubes;
    static int tubeVelocity;
    static int minTubeOffsetY;
    static int mayTubeOffsetY;
    static int distanceBetweenTubes;



    public static void initialization(Context context){
        setScreenSize(context);
        bitmapBank = new BitmapBank(context.getResources());
        setGameConstants();
        gameEngine = new GameEngine();


    }
    public static void setGameConstants(){

        AppConstants.numberOfTubes = 2;
        AppConstants.tubeVelocity = 12;
        AppConstants.minTubeOffsetY = (int)(AppConstants.gapBetweenTopAndBottomTubes/2.0);
        AppConstants.mayTubeOffsetY = AppConstants.SCREEN_HEIGHT - AppConstants.minTubeOffsetY-AppConstants.gapBetweenTopAndBottomTubes;
        AppConstants.distanceBetweenTubes = AppConstants.SCREEN_WIDTH *3/4;
        gapBetweenTopAndBottomTubes = 600;

        gravity = 3;
        AppConstants.gravity = 3;
        AppConstants.velocity_Jumped = -40;
    }
    //Return BitmapBank instance
    public static BitmapBank getBitmapBank(){
        return bitmapBank;
    }

    //Return Game engine instance
    public static GameEngine getGameEngine(){
        return gameEngine;
    }

    private static void setScreenSize(Context context){
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        AppConstants.SCREEN_WIDTH = width;
        AppConstants.SCREEN_HEIGHT = height;
    }

}
